
FROM ubuntu:trusty AS build


#
# Build Emacs
#

ARG EMACS_VERSION="emacs-27"

RUN apt-get update && apt-get -y install \
    # Deps from apt-get build-dep emacs25 on Ubuntu Bionic:
    autoconf                             \
    automake                             \
    autopoint                            \
    debhelper                            \
    dh-autoreconf                        \
    gettext                              \
    git                                  \
    intltool-debian                      \
    libarchive-zip-perl                  \
    libsigsegv2                          \
    libtimedate-perl                     \
    m4                                   \
    po-debconf                           \
    # Extra deps from my seps.sh file but I forgot how I got them:
    build-essential                      \
    libgif-dev                           \
    libgnutls28-dev                      \
    libgtk-3-dev                         \
    libjpeg-dev                          \
    libncurses5-dev                      \
    libpng-dev                           \
    libtiff5-dev                         \
    libxpm-dev                           \
    texinfo                              \
    # Just for downloading the fonts
    unzip                                \
    wget                                 \
    # CMake build dependency
    libssl-dev                           \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get -y install    \
      software-properties-common            \
    && add-apt-repository ppa:aacebedo/fasd \
    && apt-get update && apt-get -y install \
        fasd                                \
    && rm -rf /var/lib/apt/lists/*

RUN cd /tmp && wget -qO- https://github.com/Kitware/CMake/releases/download/v3.17.3/cmake-3.17.3.tar.gz | tar xzvf - \
    && cd /tmp/cmake-3.17.3                                                                                          \
    && ./bootstrap                                                                                                   \
    && make -j `nproc`                                                                                               \
    && sudo make install

RUN git clone https://github.com/mirrors/emacs.git \
        --depth=1                                  \
        --branch $EMACS_VERSION                    \
        ./emacs

RUN cd /emacs                                                    \
    && ./autogen.sh                                              \
    && ./configure --with-modules --prefix=/opt/emacs/AppDir/usr \
    && make -j `nproc`                                           \
    && make install


#
# Make it an AppImage
#

# We do not actually produce an AppImage but just an AppDir. This is because it
# requires Fuse which is not well supported in Docker
RUN wget -nv https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage \
    && chmod +x linuxdeploy-x86_64.AppImage                                                                      \
    # This icon breaks linuxdeploy
    && rm /opt/emacs/AppDir/usr/share/icons/hicolor/scalable/apps/emacs.ico                                      \
    && ./linuxdeploy-x86_64.AppImage --appimage-extract-and-run                                                  \
        --appdir /opt/emacs/AppDir                                                                               \
        -e /opt/emacs/AppDir/usr/bin/ctags                                                                       \
        -e /opt/emacs/AppDir/usr/bin/ebrowse                                                                     \
        -e /opt/emacs/AppDir/usr/bin/emacs                                                                       \
        -e /opt/emacs/AppDir/usr/bin/emacsclient                                                                 \
        -e /opt/emacs/AppDir/usr/bin/etags                                                                       \
        -d /opt/emacs/AppDir/usr/share/applications/emacs.desktop                                                \
        $(find /opt/emacs/AppDir/usr/share/icons -type f | awk '{ print "-i " $0 }' | tr '\n' ' ')               \
    && rm linuxdeploy-x86_64.AppImage


#
# Preinstall Spacemacs with packages
#

RUN mkdir -p /opt/emacs/home/skel \
    && git clone https://github.com/syl20bnr/spacemacs -b develop /opt/emacs/home/skel/.emacs.d

COPY buildfiles/.spacemacs /opt/emacs/home/skel/.

RUN ulimit -Sv 1000000                                           \
    && HOME=/opt/emacs/home/skel /opt/emacs/AppDir/usr/bin/emacs \
        --batch                                                  \
        -l /opt/emacs/home/skel/.emacs.d/init.el                 \
    && rm /opt/emacs/home/skel/.spacemacs

RUN cd $(find /opt/emacs/home/skel/.emacs.d/elpa -type d -name "vterm-*") \
    && mkdir -p build                                                     \
    && cd build                                                           \
    && cmake ..                                                           \
    && make -j `nproc`


#
# Install font Source Code Pro
#

RUN wget -nv -O source-code-pro.zip https://github.com/adobe-fonts/source-code-pro/archive/2.030R-ro/1.050R-it.zip \
    && unzip source-code-pro.zip -d source-code-pro                                                                \
    && mkdir -p /opt/emacs/fonts                                                                                   \
    && cp -v source-code-pro/*/OTF/*.otf /opt/emacs/fonts                                                          \
    && rm -rf source-code-pro{,.zip}



#
# Deploy to ADE volume image
#

FROM alpine


COPY --from=build /opt/emacs /opt/emacs

COPY skel/ /

VOLUME /opt/emacs

CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
