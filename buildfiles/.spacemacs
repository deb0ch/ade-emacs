;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  (setq-default dotspacemacs-install-packages 'all)
)

(defun dotspacemacs/init ())

(defun dotspacemacs/user-env ())

(defun dotspacemacs/user-init ()
  (setq
   ;; configuration-layer--elpa-root-directory "/opt/emacs/elpa"
))

(defun dotspacemacs/user-load ())

(defun dotspacemacs/user-config ())
