## ADE Emacs

- This project generates an Emacs (https://atom.io/) volume for [`ade`](https://gitlab.com/ApexAI/ade-cli)

### How to use

- In the `.aderc` file add, `registry.gitlab.com/apexai/atom:latest` to the list of `ADE_IMAGES`: e.g.

```
export ADE_IMAGES="
  registry.gitlab.com/group/project/ade:latest
  registry.gitlab.com/apexai/ade-atom:latest
"
```

**Note:** The following system dependencies must be installed in the base image
(`registry.gitlab.com/group/project/ade:latest` in the example above) for Emacs to launch succesfully:

```shell
apt install                 \
    curl                    \
    gnupg                   \
    gpm                     \
    imagemagick             \
    ispell                  \
    libacl1                 \
    libasound2              \
    libcanberra-gtk3-module \
    liblcms2-2              \
    libdbus-1-3             \
    libgif7                 \
    libgnutls30             \
    libgtk-3-0              \
    libjansson4             \
    libjpeg8                \
    libm17n-0               \
    libpng16-16             \
    librsvg2-2              \
    libsm6                  \
    libtiff5                \
    libx11-xcb1             \
    libxml2                 \
    libxpm4                 \
    openssh-client          \
    texinfo
```

### Installing plugins automatically

- It is possible to automatically install plugins by creating in script called `/etc/atom/atom-install-our-plugins`
in the base image
    - This script will only run the first time that the Emacs volume is loaded
- For an example of a script, see the
[AutowareAuto project](https://gitlab.com/AutowareAuto/AutowareAuto/blob/master/tools/ade_image/atom-install-our-plugins)

### Getting a specific Emacs version

- Generating a volume for a different Emacs version is as simple as creating a git tag (e.g `v1.35.0`)
- [Create an issue](https://gitlab.com/ApexAI/ade-atom/issues/new), if the desired version is not available 
