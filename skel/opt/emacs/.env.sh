#!/bin/bash


APPDIR=/opt/emacs/AppDir

VER=$(ls "$APPDIR/usr/share/emacs/" | head -n 1)


export PATH=$APPDIR/usr/bin:$PATH

export CPATH=$APPDIR/usr/include:$CPATH

export INFOPATH=$APPDIR/usr/share/info:$INFOPATH
export MANPATH=$APPDIR/usr/share/man:$MANPATH

export EMACSPATH=$APPDIR/usr/share/emacs/$VER
export EMACSDATA=$EMACSPATH/etc
export EMACSDOC=$EMACSPATH/etc
export EMACSLOADPATH=$EMACSPATH/site-lisp:$EMACSPATH/lisp:$EMACSPATH/lisp/emacs-lisp
